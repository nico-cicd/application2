FROM python:3.12-rc-slim-bullseye

WORKDIR /code

COPY ./src ./src
COPY ./requirements.txt ./
COPY ./init_bdd.sh ./

RUN apt -y update \
&& apt -y upgrade 

RUN apt install -y libpq5 curl

RUN pip install --no-cache-dir -r requirements.txt

CMD ["bash", "init_bdd.sh"]
