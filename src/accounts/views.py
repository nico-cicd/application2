from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

def login_user(requests):
    if requests.method == 'POST':
        username = requests.POST["username"]
        password = requests.POST["password"]

        user = authenticate(requests, username=username, password=password)

        if user is not None:
            login(requests, user)
            return redirect('accueil')
        else:
            messages.info(requests, "Identifiant ou mot de passe incorrect. Dommage !")

    form = AuthenticationForm()
    context = {"form": form}
    return render(requests, "login.html", context)

def logout_user(requests):
    logout(requests)
    return redirect('login_user')

def register_user(requests):
    if requests.method == 'POST':
        form = UserCreationForm(requests.POST)

        if form.is_valid():
            form.save()
            return redirect("login_user")
    else:
        form = UserCreationForm()

    context = {"form": form}
    return render(requests, "register.html", context)





