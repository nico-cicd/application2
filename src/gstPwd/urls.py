"""
URL configuration for gstPwd project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path, include

from backend.urls import router as backend_rooter
from rest_framework import routers

from frontend.views import index, accueil, ajouter_credential, supprimer_credential, modifier_credential
from accounts.views import login_user, logout_user, register_user

router = routers.DefaultRouter()
router.registry.extend(backend_rooter.registry)

app_name = 'gstPwd'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls), name='api'),
    path('', view=index, name='index'),
    path('login/', view=login_user, name='login_user'),
    path('logout/', view=logout_user, name='logout_user'),
    path('register/', view=register_user, name='register_user'),
    path('accueil/', view=accueil, name='accueil'),
    path('ajouter_credential/', view=ajouter_credential, name='ajouter_credential'),
    path('supprimer_credential/<int:credential_id>/', view=supprimer_credential, name='supprimer_credential'),
    path('modifier_credential/<int:credential_id>/', view=modifier_credential, name='modifier_credential'),
]
