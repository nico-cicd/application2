from rest_framework import viewsets
from backend.models import Credential, Utilisateur
from backend.serializer import CredentialSerializer, UtilisateurSerializer

class CredentialViewSet(viewsets.ModelViewSet):
    queryset = Credential.objects.all()
    serializer_class = CredentialSerializer

class UtilisateurViewSet(viewsets.ModelViewSet):
    queryset = Utilisateur.objects.all()
    serializer_class = UtilisateurSerializer
