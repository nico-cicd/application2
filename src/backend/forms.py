from django import forms
from backend.models import Credential

class CredentialForm(forms.ModelForm):
    class Meta:
        model = Credential
        fields = ['nom_du_site', 'identifiant', 'mot_de_passe']
        labels = {
            'nom_du_site': 'Nom du site',
            'identifiant': 'Identifiant',
            'mot_de_passe': 'Mot de passe'
            }

