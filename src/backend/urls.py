from rest_framework import routers
from backend.views import CredentialViewSet, UtilisateurViewSet

router = routers.DefaultRouter()
router.register('credential', CredentialViewSet)
router.register('utilisateur', UtilisateurViewSet)
