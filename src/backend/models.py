from django.db import models

class Credential(models.Model):
    nom_du_site = models.CharField(max_length=64)
    identifiant = models.CharField(max_length=32)
    mot_de_passe = models.CharField(max_length=32)

class Utilisateur(models.Model):
    nom_utilisateur = models.CharField(max_length=128, default='lapin')
