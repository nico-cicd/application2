# Generated by Django 4.2.4 on 2023-08-05 21:19

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='credential',
            name='info',
        ),
    ]
