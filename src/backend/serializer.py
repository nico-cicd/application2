from rest_framework import serializers
from backend.models import Credential, Utilisateur


class CredentialSerializer(serializers.ModelSerializer):

    class Meta:
        model = Credential
        fields = '__all__'

class UtilisateurSerializer(serializers.ModelSerializer):

    class Meta:
        model = Utilisateur
        fields = '__all__'
