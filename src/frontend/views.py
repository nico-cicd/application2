from django.shortcuts import render, redirect

from backend.models import Credential

from backend.forms import CredentialForm

def index(requests):
    return redirect("login_user")

def accueil(requests):
    context = {
        "credentials": Credential.objects.all()
        }
    return render(requests, "accueil.html", context)

def ajouter_credential(requests):
    if requests.method == 'POST':
        form = CredentialForm(requests.POST)

        if form.is_valid():
            form.save()
            return redirect("accueil")
    else:
        form = CredentialForm()
    
    context = {
        "form": form,
        "credentials": Credential.objects.all()
        }
    return render(requests, "gestion_mot_de_passe.html", context)

def supprimer_credential(requests, credential_id):
    credential = Credential.objects.get(pk=credential_id)
    credential.delete()
    return redirect("accueil")

def modifier_credential(requests, credential_id):
    credential = Credential.objects.get(pk=credential_id)
    if requests.method == 'POST':
        form = CredentialForm(requests.POST, instance=credential)

        if form.is_valid():
            form.save()
            return redirect("accueil")
    else:
        form = CredentialForm(instance=credential)
    
    context = {
        "form": form,
        "credentials": Credential.objects.all()
        }
    return render(requests, "gestion_mot_de_passe.html", context)
