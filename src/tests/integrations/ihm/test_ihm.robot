*** Settings ***
Library           Browser

*** Variables ***
${headless}    ${True}
${channel}        chrome
${url}    http://app1:8000/    #http://www.ausy.fr/

*** Keywords ***
Ouvrir navigateur
    Browser.New Browser                                        browser=chromium
    ...                                                        headless=${headless}
    ...                                                        channel=${channel}
    Browser.New Page                                           url=${url}
    Builtin.Sleep    time_=5
    ...      reason=Je teste !

*** Test Cases ***
Ouverture navigateur
    Ouvrir navigateur