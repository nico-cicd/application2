*** Settings ***
Library    Browser
Library    Collections

*** Keywords ***
Requete - Application activée
    [Tags]    log
    [Arguments]    ${requete_url}
    Browser.New Page                                           url=${requete_url}
    ${headersDict}                                             Builtin.Create Dictionary
    Collections.Set To Dictionary                              ${headersDict}
    ...                                                        Accept    application/json
    ...                                                        Content-Type    application/json
    &{resultat}                                                Browser.Http
    ...                                                        url=${requete_url}
    ...                                                        method=GET
    ...                                                        body=${EMPTY}
    ...                                                        headers=${headersDict}
    Builtin.Should Be Equal                                    first="${resultat.status}"
    ...                                                        second="200"
    ...                                                        msg=Il y a le message suivant : ${resultat.body}
    Builtin.Log                                                ${resultat.status}
    Builtin.Log                                                ${resultat.body}
    RETURN    ${resultat.body}
    [Teardown]        Browser.Close Page

*** Test Cases ***
Test requete
    Requete - Application activée    requete_url=http://52.47.174.206/login/