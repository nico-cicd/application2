import requests
import json

def post():
    url = 'http://app1:8000/api/credential/'
    body = {
        "nom_du_site": "Moto",
        "identifiant": "Lapin",
        "mot_de_passe": "Catapulte",
        }
    x = requests.post(url, data=body)
    return x

def get():
    url = 'http://app1:8000/api/credential/'
    reponse = requests.request("GET", url)
    data = json.loads(reponse.text)
    print(data)
    return data


get()
post()